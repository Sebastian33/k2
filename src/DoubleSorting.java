import java.util.*;

/**
 * Comparison of sorting methods. The same array of double values is
 * used for all methods.
 * 
 * @author Jaanus
 * @version 1.0
 * @since 1.6
 */
public class DoubleSorting {

   /** maximal array length */
   static final int MAX_SIZE = 512000;

   /** number of competition rounds */
   static final int NUMBER_OF_ROUNDS = 4;

   /**
    * Main method.
    * 
    * @param args
    *           command line parameters
    */
   public static void main(String[] args) {
      final double[] origArray = new double[MAX_SIZE];
      Random generator = new Random();
      for (int i = 0; i < MAX_SIZE; i++) {
         origArray[i] = generator.nextDouble()*1000.;
      }
      int rightLimit = MAX_SIZE / (int) Math.pow(2., NUMBER_OF_ROUNDS);

      // Start a competition
      for (int round = 0; round < NUMBER_OF_ROUNDS; round++) {
         double[] acopy;
         long stime, ftime, diff;
         rightLimit = 2 * rightLimit;
         System.out.println();
         System.out.println("Length: " + rightLimit);

         acopy = Arrays.copyOf(origArray, rightLimit);
         stime = System.nanoTime();
         binaryInsertionSortKolmasLahendus(acopy);
         ftime = System.nanoTime();
         diff = ftime - stime;
         System.out.printf("%34s%11d%n", "Binary insertion sort (praktikum): time (ms): ", diff / 1000000);
         checkOrder(acopy);

         acopy = Arrays.copyOf(origArray, rightLimit);
         stime = System.nanoTime();
         binaryInsertionSort(acopy);
         ftime = System.nanoTime();
         diff = ftime - stime;
         System.out.printf("%34s%11d%n", "Binary insertion sort (minu): time (ms): ", diff / 1000000);
         checkOrder(acopy);

         acopy = Arrays.copyOf(origArray, rightLimit);
         stime = System.nanoTime();
         binaryInsertionSortPaarilise(acopy);
         ftime = System.nanoTime();
         diff = ftime - stime;
         System.out.printf("%34s%11d%n", "Binary insertion sort (paarilise): time (ms): ", diff / 1000000);
         checkOrder(acopy);


      }
   }

   /**
    * Binary insertion sort. Credit: https://www.geeksforgeeks.org/binary-insertion-sort/
    *
    * @param a
    *           array to be sorted
    *
    */
   public static void binaryInsertionSort(double[] a) {
      for (int i = 1; i < a.length; i++) {
         double x = a[i];
         int j = Math.abs(Arrays.binarySearch(a, 0, i, x) + 1);
         System.arraycopy(a, j, a, j + 1, i - j);
         a[j] = x;
      }
   }

   public static void binaryInsertionSortKolmasLahendus(double[] a) {
      // https://enos.itcollege.ee/~japoia/algoritmid/searchsort.html
      if (a.length < 2) return;
      for (int i = 1; i < a.length; i++) {
         double b = a[i];

         int j = 0;
         int l = 0;              // vasakpoolne otspunkt
         int r = i - 1;   // parempoolne otspunkt
         while (l <= r) {
            j = (l + r) / 2;

            if (b > a[j]) {
               if (i == j + 1) { // Kontrollime, kas järgmine nihutamine viitaks juba ette antud elendile
                  j++;
                  break;
               } else {
                  l = j + 1; // vasak otspunkt nihkub paremale
               }
            } else {
               if (j - 1 >= 0 && a[j - 1] > b) { // Kontrollime, kas j on veel mõtet vähendada
                  r = j - 1; // parem otspunkt nihkub vasakule
               } else {
                  break;
               }
            }
         }

         System.arraycopy(a, j, a, j + 1, i - j); // Teeme b jaoks ruumi
         a[j] = b; // pistame b õigesse kohta
      }
   }

   public static void binaryInsertionSortPaarilise(double[] a) {
      // https://enos.itcollege.ee/~japoia/algoritmid/searchsort.html
      if (a.length < 2) return;
      for (int i = 1; i < a.length; i++) {
         double b = a[i];

         int j = binarySearch(a, b, i); // Otsime b jaoks koha

         System.arraycopy(a, j, a, j + 1, i - j); // Teeme b jaoks ruumi
         a[j] = b; // pistame b õigesse kohta
      }
   }

   static public int binarySearch (double[] a, double e, int index) {

      int j;
      int l = 0;              // vasakpoolne otspunkt
      int r = index - 1;   // parempoolne otspunkt
      while (l <= r) {
         j = (l + r) / 2;

         if (e > a[j]) {
            if (index == j + 1) { // Kontrollime, kas järgmine nihutamine viitaks juba ette antud elendile
               return j + 1;
            } else {
               l = j + 1; // vasak otspunkt nihkub paremale
            }
         } else {
            if (j - 1 >= 0 && a[j - 1] > e) { // Kontrollime, kas j on veel mõtet vähendada
               r = j - 1; // parem otspunkt nihkub vasakule
            } else {
               return j;
            }
         }
      }
      return -1;
   }

   /**
    * Check whether an array is ordered.
    * 
    * @param a
    *           sorted (?) array
    * @throws IllegalArgumentException
    *            if an array is not ordered
    */
   static void checkOrder(double[] a) {
      if (a.length < 2)
         return;
      for (int i = 0; i < a.length - 1; i++) {
         if (a[i] > a[i + 1])
            throw new IllegalArgumentException(
                  "array not ordered: " + "a[" + i + "]=" + a[i] + " a[" + (i + 1) + "]=" + a[i + 1]);
      }
   }

}

